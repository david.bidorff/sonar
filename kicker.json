{
  "name": "SonarQube",
  "description": "GitLab CI template for [SonarQube](https://www.sonarqube.org/)",
  "template_path": "templates/gitlab-ci-sonar.yml",
  "kind": "analyse",
  "variables": [
    {
      "name": "SONAR_SCANNER_IMAGE",
      "description": "The Docker image used to run [sonar-scanner](https://docs.sonarqube.org/latest/analysis/scan/sonarscanner/)",
      "default": "sonarsource/sonar-scanner-cli:latest"
    },
    {
      "name": "SONAR_URL",
      "type": "url",
      "description": "SonarQube server url",
      "mandatory": true
    },
    {
      "name": "SONAR_AUTH_TOKEN",
      "description": "SonarQube authentication [token](https://docs.sonarqube.org/latest/user-guide/user-token/) (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_LOGIN",
      "description": "SonarQube login (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_PASSWORD",
      "description": "SonarQube password (depends on your authentication method)",
      "secret": true
    },
    {
      "name": "SONAR_BASE_ARGS",
      "description": "SonarQube [analysis arguments](https://docs.sonarqube.org/latest/analysis/analysis-parameters/)",
      "default": "-Dsonar.host.url=${SONAR_URL} -Dsonar.projectKey=${CI_PROJECT_PATH_SLUG} -Dsonar.projectName=${CI_PROJECT_PATH} -Dsonar.projectBaseDir=. -Dsonar.links.homepage=${CI_PROJECT_URL} -Dsonar.links.ci=${CI_PROJECT_URL}/-/pipelines -Dsonar.links.issue=${CI_PROJECT_URL}/-/issues",
      "advanced": true
    },
    {
      "name": "SONAR_GITLAB_TOKEN",
      "description": "GitLab API access token. When set, activates the [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties) integration, and enables SonarQube [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/)",
      "secret": true
    },
    {
      "name": "SONAR_BRANCH_ANALYSIS_DISABLED",
      "description": "Set to disable automatic [Pull Request Analysis](https://docs.sonarqube.org/latest/analysis/pull-request/) and [Branch Analysis](https://docs.sonarqube.org/latest/branches/overview/)",
      "type": "boolean"
    },
    {
      "name": "SONAR_GITLAB_ARGS",
      "description": "Extra arguments to use with [Sonar GitLab plugin](https://github.com/gabrie-allaigre/sonar-gitlab-plugin/#plugins-properties)",
      "default": "-Dsonar.gitlab.url=${CI_SERVER_URL} -Dsonar.gitlab.user_token=${SONAR_GITLAB_TOKEN} -Dsonar.gitlab.project_id=${CI_PROJECT_ID} -Dsonar.gitlab.commit_sha=${CI_COMMIT_SHA} -Dsonar.gitlab.ref_name=${CI_COMMIT_REF_NAME}",
      "advanced": true
    },
    {
      "name": "SONAR_AUTO_ON_DEV_DISABLED",
      "description": "When set, SonarQube analysis becomes **manual** on development branches (automatic otherwise)",
      "type": "boolean"
    }
  ]
}
